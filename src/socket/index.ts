import { Server } from 'socket.io';
import { texts } from "../data";
import { MAXIMUM_USERS_FOR_ONE_ROOM, SECONDS_TIMER_BEFORE_START_GAME } from "./config";

type UserType = {
	userName: string,
	isReady: boolean
};

type RoomType = {
	roomName: string,
	users: UserType[],
	isStartedGame: boolean
};

const usernames: string[] = [];
const rooms: RoomType[] = [];

export default (io: Server) => {
	io.use(function (socket, next) {
		const username = String(socket.handshake.query.username);
		console.log("-------------------USE user try to login", socket.handshake.query.username, JSON.stringify(usernames))

		if (!usernames.includes(username) && username !== "null") {
			console.log("------------logged-in", username)
			return next();
		}

		return next(new Error(`User with name ${username} already exists`));
	});

	io.on('connection', socket => {
		const username = String(socket.handshake.query.username);

		const quitRoom = (roomName: string) => {
			const room = rooms.find(room => room.roomName === roomName);
			if (!room) return;

			if (room.users.every(user => user.isReady)) {

			}

			room.users.splice(room.users.findIndex(item => item.userName === username), 1);
			socket.leave(room.roomName);
			io.emit("room-updated", room);
			io.to(room.roomName).emit("user-ready", room.users);

			if (room.users.length === 0) {
				rooms.splice(rooms.findIndex(item => item.roomName === roomName), 1);
				io.emit("update-rooms", getAvailableRooms());
			} else if (room.users.length > 1 && room.users.every(usr => usr.isReady)) {
				io.to(room.roomName).emit("start-game-timer", SECONDS_TIMER_BEFORE_START_GAME);
				setTimeout(() => io.to(room.roomName).emit("start-game", texts[0]), 5000);
				room.isStartedGame = true;
			}
		};

		const getAvailableRooms = () => {
			return rooms.filter(room => !room.isStartedGame && room.users.length < MAXIMUM_USERS_FOR_ONE_ROOM);
		}

		usernames.push(username);
		console.log("-------------------CONNECTION", username);

		socket.emit("update-rooms", getAvailableRooms());

		socket.on("create-room", (roomName) => {
			if (rooms.some(room => room.roomName === roomName)) {
				socket.emit("error-create-room", `Room with name ${roomName} already exists`);
				console.log("----------create room error")
			} else {
				const room: RoomType = {
					roomName,
					users: [{
						userName: username,
						isReady: false
					}],
					isStartedGame: false
				};
				rooms.push(room);
				socket.join(roomName);
				socket.emit("joined-room", room);
				io.emit("room-created", room);
				console.log("----------room created", room)
			}
		});

		socket.on("join-room", roomName => {
			const room = rooms.find(room => room.roomName === roomName);
			if (!room) return;
			if (room.users.some(item => item.userName === username)) {
				socket.emit("error-create-room", "You are already in the room");
				return;
			}
			if (room.users.length === MAXIMUM_USERS_FOR_ONE_ROOM) {
				socket.emit("error-create-room", "The room is full");
				return;
			}
			room.users.push({ userName: username, isReady: false });
			socket.join(room.roomName);
			io.to(room.roomName).emit("room-updated", room);
			socket.emit("joined-room", room);
			io.to(room.roomName).emit("user-ready", room.users);
		});

		socket.on("quit-room", (roomName) => {
			quitRoom(roomName);
		});

		socket.on("toggle-ready", () => {
			rooms.forEach(item => {
				const user = item.users.find(usr => usr.userName === username);
				if (user) {
					user.isReady = !user.isReady;
					io.to(item.roomName).emit("user-ready", item.users);
				}

				if (item.users.length > 1 && item.users.every(usr => usr.isReady)) {
					io.to(item.roomName).emit("start-game-timer", SECONDS_TIMER_BEFORE_START_GAME);
					setTimeout(() => io.to(item.roomName).emit("start-game", texts[0]), 5000);
					item.isStartedGame = true;
				}
			})
		});

		socket.on('disconnect', (e) => {
			console.log('disconnected', username);
			usernames.splice(usernames.indexOf(username), 1);
			rooms.forEach(room => quitRoom(room.roomName));
		});
	});
};
