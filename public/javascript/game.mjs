import { AlertMessage } from "./enums/alert-messages.enum.js";
import { appendRoomElement } from "./views/room.mjs";
import { appendUserElement } from "./views/user.mjs";

const username = sessionStorage.getItem("username");

if (!username) {
	window.location.replace("/login");
}

const socket = io("", { query: { username } });

socket.on('connect_error', (e) => {
	alert(e);
});

socket.on("error-create-room", function (msg) {
	alert(msg);
});

const createRoomButton = document.getElementById("add-room-btn");
createRoomButton.onclick = () => {
	console.log("click create room");
	const roomName = prompt(AlertMessage.CREATE_ROOM);

	if (roomName !== null && roomName !== "") {
		socket.emit("create-room", roomName);
	}
}

const onJoin = e => {
	console.log(username, "try to join room", e.target.dataset.roomName)
	socket.emit("join-room", e.target.dataset.roomName);
}

const createRoom = ({ roomName, users }) => {
	appendRoomElement({
		name: roomName,
		numberOfUsers: users.length,
		onJoin,
	});
	console.log("created room", roomName);
}

const updateRoom = (room) => {
	const element = document.querySelector(`[data-room-name="${room.roomName}"]`);
	element?.remove();
	console.log("element remove", element);
	createRoom(room);
}

const switchRoom = () => {
	const roomsPage = document.getElementById("rooms-page");
	roomsPage.classList.toggle("display-none");

	const gamePage = document.getElementById("game-page");
	gamePage.classList.toggle("display-none");

	timer.classList.add("display-none");
	textContainer.classList.add("display-none");
	buttonReady.classList.remove("display-none");
	buttonReady.innerHTML = "READY";
}

const updateRooms = (rooms) => {
	document.querySelector('#rooms-wrapper').innerHTML = "";
	rooms.reverse();
	rooms.forEach(updateRoom);
}

socket.on("room-updated", updateRoom);
socket.on("room-created", r => createRoom(r));
socket.on("update-rooms", updateRooms);

const updateUsers = (users) => {
	document.querySelector('#users-wrapper').innerHTML = "";

	users.forEach(user => {
		appendUserElement({
			username: user.userName,
			ready: user.isReady,
			isCurrentUser: user.userName === username
		});
	});
}

socket.on("joined-room", (room) => {
	switchRoom();
	updateUsers(room.users);
	document.getElementById("room-name").innerHTML = room.roomName;
});

socket.on("user-ready", (users) => {
	updateUsers(users);
});

const backToRoomsButton = document.getElementById("quit-room-btn");
backToRoomsButton.onclick = () => {
	const room = document.getElementById("room-name").innerHTML;
	socket.emit("quit-room", room);
	switchRoom();
}

const buttonReady = document.getElementById("ready-btn");
buttonReady.onclick = (e) => {
	socket.emit("toggle-ready");
	buttonReady.innerHTML = buttonReady.innerHTML === "READY" ? "NOT READY" : "READY";
}

const timer = document.getElementById("timer");
const gameTimer = document.getElementById("game-timer");
const textContainer = document.getElementById("text-container");

socket.on("start-game-timer", (seconds) => {
	timer.classList.toggle("display-none");
	buttonReady.classList.toggle("display-none");
	timer.innerHTML = seconds;

	let sec = seconds - 1;
	const interval = setInterval(() => {
		timer.innerHTML = sec--;

		if (sec === 0) {
			clearInterval(interval);
		}
	}, 1000);
});

const keyDown = (e) => {
	console.log(e.key)

	const userProgress = document.getElementsByClassName("user-progress");
	userProgress[0].style.width = "10%";
}

socket.on("start-game", (text) => {
	timer.classList.toggle("display-none");
	textContainer.classList.toggle("display-none");
	textContainer.innerHTML = text;

	gameTimer.classList.toggle("display-none");
	document.addEventListener("keydown", keyDown); 
});

