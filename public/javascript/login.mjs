const username = sessionStorage.getItem('username');
const conf = { query: { username } };
const socket = io("", conf);

socket.on('connect_error', (e) => {
	if (conf.query.username !== null) {
		console.log("connect_error", e)
		alert(e);
	}
});

socket.on("connect", () => {
	console.log("user conected!", conf.query.username)
	sessionStorage.setItem('username', conf.query.username);
	socket.disconnect();
	window.location.replace('/game');
});


const submitButton = document.getElementById('submit-button');
const input = document.getElementById('username-input');

const getInputValue = () => input.value;

const onClickSubmitButton = () => {
	const inputValue = getInputValue();

	if (inputValue !== null && inputValue !== "") {
		console.log("not null and not empty string");
		conf.query.username = inputValue;
		socket.connect();
	}
};

const onKeyUp = ev => {
	const enterKeyCode = 13;
	if (ev.keyCode === enterKeyCode) {
		submitButton.click();
	}
};

submitButton.addEventListener('click', onClickSubmitButton);
window.addEventListener('keyup', onKeyUp);
